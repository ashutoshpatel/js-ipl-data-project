// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    allMatchesData.push(data);
})
.on('end', () => {
    
    // call the countNumberOfMatchesOfYears function
    const result = countNumberOfMatchesOfYears(allMatchesData);
    const path = '../public/output/matchesPerYear.json';

    // store the data in JSON formate
    fs.writeFileSync(path, JSON.stringify(result, null, 1));
});

// declare countNumberOfMatchesOfYears function
function countNumberOfMatchesOfYears(allMatchesData){
    // create empty result object to store final result
    const result = {};

    // use for-in loop to iterate on allMatchesData
    for(let key in allMatchesData){
        // get the currentSeason
        const currentSeason = allMatchesData[key].season;
        // if the currentSeason has already in result object then update it's value by 1
        if(result[currentSeason]){
            result[currentSeason] = result[currentSeason] + 1;
        }
        else{
            // if it's not present so add them with value 1
            result[currentSeason] = 1;
        }
    }

    // return the result object
    return result;
}