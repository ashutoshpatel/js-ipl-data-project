// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    allMatchesData.push(data);
})
.on('end', () => {
    
    // call the findTheNumberOfEachTeamWonTossAndMatchAlso function
    const result = findTheNumberOfEachTeamWonTossAndMatchAlso(allMatchesData);
    const path = '../public/output/theNumberOfEachTeamWonTossAndMatch.json';

    // store the data in JSON formate
    fs.writeFileSync(path, JSON.stringify(result, null, 1));
});

// declare findTheNumberOfEachTeamWonTossAndMatchAlso function
function findTheNumberOfEachTeamWonTossAndMatchAlso(allMatchesData){
    // create result object to store the answer
    const result = {};

    // use for-in loop to iterate on allMatchesData array
    for(let key in allMatchesData){
        // get the current match toss winnner team
        const tossWinner = allMatchesData[key].toss_winner;
        // get the current match winnner team
        const matchWinner = allMatchesData[key].winner;

        // if the tossWinner and matchWinner team are same
        if(tossWinner === matchWinner){
            // now check that the team is already present in result object or not
            if(result.hasOwnProperty(tossWinner)){
                // if yes then simply update it's value by one
                result[tossWinner]++;
            }
            else{
                // if the team not present in result object then add it with value 1
                result[tossWinner] = 1;
            }
        }
    }

    // return the result object
    return result;
}