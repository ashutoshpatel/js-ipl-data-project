// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];
const allDeliveriesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    // add only 2016 season data in allMatchesData array
    if(data.season === '2016'){
        allMatchesData.push(data);
    }
})
.on('end', () => {

    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (deliveries) => {
        allDeliveriesData.push(deliveries);
    })
    .on('end', () => {
        // call the calculateExtraRunsPerTeamInYear2016 function
        const result = calculateExtraRunsPerTeamInYear2016(allMatchesData, allDeliveriesData);
        const path = '../public/output/extraRunsPerTeamInYear2016.json';

        // store the data in JSON formate
        fs.writeFileSync(path, JSON.stringify(result, null, 1));
    });
});

// declare calculateExtraRunsPerTeamInYear2016 function
function calculateExtraRunsPerTeamInYear2016(allMatchesData, allDeliveriesData){

    // create extraRunsPerTeam object to store the result
    let extraRunsPerTeam = {};

    // use for-in loop to iterate on allMatchesData array
    for(let matcheKey in allMatchesData){

        // get the current matcheId
        const matcheId = allMatchesData[matcheKey].id;
        // again use for-in loop to iterate on allDeliveriesData array
        for(let deliveryKey in allDeliveriesData){
            // get the current deliverMatchId
            const deliverMatchId = allDeliveriesData[deliveryKey].match_id;
            
            // if the current matcheId and current deliverMatchId is same
            if(matcheId === deliverMatchId){
                // get the current bowlingTeam
                const bowlingTeam = allDeliveriesData[deliveryKey].bowling_team;
                // get the extraRuns and convert into interger
                const extraRuns = Number(allDeliveriesData[deliveryKey].extra_runs);

                // if the extraRuns is not equal to 0 means we have some extra run
                if(extraRuns !== 0){
                    // now check that current bowlingTeam is already present in extraRunsPerTeam object
                    if(extraRunsPerTeam.hasOwnProperty(bowlingTeam)){
                        // is yes then update the old extraRuns with new extraRuns
                        extraRunsPerTeam[bowlingTeam] = extraRunsPerTeam[bowlingTeam] + extraRuns;
                    }
                    else{
                        // if the current bowlingTeam is not present in object then add the bowlingTeam with extraRuns
                        extraRunsPerTeam[bowlingTeam] = extraRuns;
                    }
                }
            }
        }
    }
    
    // return the extraRunsPerTeam object
    return extraRunsPerTeam;
}