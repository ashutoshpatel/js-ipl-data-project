// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    allMatchesData.push(data);
})
.on('end', () => {

    // call the numberOfMatchesWonPerTeamPerYear function
    const result = numberOfMatchesWonPerTeamPerYear(allMatchesData);
    const path = '../public/output/matchesWonPerTeamPerYear.json';

    // store the data in JSON formate
    fs.writeFileSync(path, JSON.stringify(result, null, 1));
});

// declare numberOfMatchesWonPerTeamPerYear function
function numberOfMatchesWonPerTeamPerYear(allMatchesData){

    // create empty result object to store final result
    const result = {};

    // use for-in loop to iterate on allMatchesData
    for(let key in allMatchesData){
        // get the currentSeason
        const currentSeason = allMatchesData[key].season;
        // get the winner
        const winner = allMatchesData[key].winner;

        // if the winner is not empty means we have a winner
        if(winner !== ""){
            // check the winner is already present in result object
            if(result.hasOwnProperty(winner)){
                // if yes then now check that for those winner the currentSeason is present in the object
                if(result[winner][currentSeason]){
                    // if yes then get the old wining count
                    const oldWiningCount = result[winner][currentSeason];
                    // update the data with old wining count + 1
                    result[winner][currentSeason] = oldWiningCount + 1;
                }
                else{
                    // if the winner object is present but the currentSeason is not present
                    // then add currentSeason in object with 1 value
                    result[winner][currentSeason] = 1;
                }
            }
            else{
                // if the winner is not present in object then add with currentSeason and value 1
                result[winner] = {[currentSeason] : 1};
            }
        }
        else{ // if the winner is empty means the match not have a result
            // if the No Result Matches key are already present in result object
            if(result['No Result Matches']){
                // check the currentSeason is present in that object
                if(result['No Result Matches'][currentSeason]){
                    // if yes then get old value
                    const oldWiningCount = result['No Result Matches'][currentSeason];
                    // now update that with old value + 1
                    result['No Result Matches'][currentSeason] = oldWiningCount + 1;
                }
                else{
                    // if the No Result Matches key present but currentSeason is not present
                    // add the currentSeason in object with value 1
                    result['No Result Matches'][currentSeason] = 1;
                }
            }
            else{
                // if the No Result Matches key not present in object
                // then add with currentSeason and value 1
                result['No Result Matches'] = {[currentSeason] : 1};
            }
        }
    }

    // return the result object
    return result;
}