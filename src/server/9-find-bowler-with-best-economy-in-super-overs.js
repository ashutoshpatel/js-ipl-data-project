// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allDeliveryData = [];

fs.createReadStream('../data/deliveries.csv')
.pipe(csv())
.on('data', (data) => {
    allDeliveryData.push(data);
})
.on('end', () => {
    
    // call the findBowlerWithBestEconomyInSuperOvers function
    const result = findBowlerWithBestEconomyInSuperOvers(allDeliveryData);
    const path = '../public/output/BowlerWithBestEconomyInSuperOvers.json';

    // store the data in JSON formate
    fs.writeFileSync(path, JSON.stringify(result, null, 1));
});

// declare findBowlerWithBestEconomyInSuperOvers function
function findBowlerWithBestEconomyInSuperOvers(allDeliveryData){

    // create object to store the bowler name and runs and balls
    const bowlerDetails = {};

    // iterate on allDeliveryData array
    for(let key in allDeliveryData){

        // check the ball is come inside super over or not
        if(allDeliveryData[key].is_super_over !== '0'){
            // if it's super over
            // get the bowler name
            const bowlerName = allDeliveryData[key].bowler;
            // get the bye run
            const byeRuns = Number(allDeliveryData[key].bye_runs);
            // get the legbye run
            const legbyeRuns = Number(allDeliveryData[key].legbye_runs);
            // get the total run
            let run = Number(allDeliveryData[key].total_runs);
            // subtract the byeRuns and legbyeRuns from total run
            run = run - byeRuns - legbyeRuns;
            // check the bowler name is already present in object
            if(bowlerDetails[bowlerName]){
                // if yes get the run and update run
                bowlerDetails[bowlerName]['runs'] += run;
                // update the balls by one
                bowlerDetails[bowlerName]['balls']++;
            }
            else{
                // if the bowler name is not present in object
                // add bowler in object with runs and balls 1
                bowlerDetails[bowlerName] = { 'runs' : run , 'balls' : 1};
            }
        }
    }

    let currentEconomy = 0;
    let bowlerName;
    // iterate on bowlerDetails object to get best economy
    for(let key in bowlerDetails){

        // get the current bowler total run
        const run = Number(bowlerDetails[key]['runs']);
        // get the current bowler total balls
        const balls = Number(bowlerDetails[key]['balls']) / 6;
        // calculate the economy
        const economy = run / balls;

        // if the currentEconomy is 0 or economy is smaller than currentEconomy
        if(currentEconomy===0 || economy < currentEconomy){
            // then update the currentEconomy with new economy
            currentEconomy = economy;
            // and update the bowlerName with current bowler name 
            bowlerName = key;
        }
    }

    // return the final result
    return {[bowlerName] : currentEconomy};
}