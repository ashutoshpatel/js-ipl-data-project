// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];
const allDeliveriesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    allMatchesData.push(data);
})
.on('end', () => {

    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (deliveries) => {
        allDeliveriesData.push(deliveries);
    })
    .on('end', () => {
        // call the findStrikeRateOfBatsman function
        const result = findStrikeRateOfBatsman(allMatchesData, allDeliveriesData);
        const path = '../public/output/strikeRateOfBatsman.json';

        // store the data in JSON formate
        fs.writeFileSync(path, JSON.stringify(result, null, 1));
    });
});

// declare findStrikeRateOfBatsman function
function findStrikeRateOfBatsman(allMatchesData, allDeliveriesData){

    // create result object to store final answer
    const result = {};
    // create storeStrikeRate object to store totalRun and totalBalls
    const storeStrikeRate = {};

    for(let matchKey in allMatchesData){
        // get current season
        const currentSeason = allMatchesData[matchKey].season;

        for(let deliveryKey in allDeliveriesData){
            if(allMatchesData[matchKey].id === allDeliveriesData[deliveryKey].match_id){
                // get current batsman
                const currentBatsman = allDeliveriesData[deliveryKey].batsman;
                // get batsman runs
                const batsmanRuns = Number(allDeliveriesData[deliveryKey].batsman_runs);

                // check the current season present in storeStrikeRate object or not
                if(storeStrikeRate.hasOwnProperty(currentSeason)){
                    // check the currentBatsman is present in currentSeason object
                    if(storeStrikeRate[currentSeason][currentBatsman]){
                        // get the old runs for current batsman
                        const oldRuns = storeStrikeRate[currentSeason][currentBatsman]['runs'];
                        // update the runs
                        storeStrikeRate[currentSeason][currentBatsman]['runs'] = oldRuns + batsmanRuns;
                        // update the balls by one
                        storeStrikeRate[currentSeason][currentBatsman]['balls']++;
                    }
                    else{
                        // if the current season is present but the batsman is not present
                        // then add with runs and balls 1
                        storeStrikeRate[currentSeason][currentBatsman] = { 'runs' : batsmanRuns , 'balls' : 1 };
                    }
                }
                else{
                    // if the current season is not present in object
                    // then add the season with batsman runs and balls 1
                    storeStrikeRate[currentSeason] = { [currentBatsman] : { 'runs' : batsmanRuns , 'balls' : 1 } };
                }
            }
        }
    }

    // iteate on storeStrikeRate and find the strike rate
    for(let key in storeStrikeRate){
        // iterate on a season
        for(let playerName in storeStrikeRate[key]){
            // get the current player runs
            const currentPlayerRuns = Number(storeStrikeRate[key][playerName]['runs']);
            // get the current player balls 
            const currentPlayerBalls = Number(storeStrikeRate[key][playerName]['balls']);

            // calculate the strike rate
            const strikeRateOfPlayer = ((currentPlayerRuns / currentPlayerBalls) * 100).toFixed(2);

            // add the key(season) is present in result object
            if(result[key]){
                // add the player with his strike rate
                result[key][playerName] = strikeRateOfPlayer;
            }
            else{
                // if the key(season) not present in result object
                // add season with player name and strike rate
                result[key] = { [playerName] : strikeRateOfPlayer};
            }
        }
    }

    // return the result object
    return result;
}