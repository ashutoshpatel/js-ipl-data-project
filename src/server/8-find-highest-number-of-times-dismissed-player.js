// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allDeliveryData = [];

fs.createReadStream('../data/deliveries.csv')
.pipe(csv())
.on('data', (data) => {
    allDeliveryData.push(data);
})
.on('end', () => {
    
    // call the findHighestNumberOfTimesDismissedPlayer function
    const result = findHighestNumberOfTimesDismissedPlayer(allDeliveryData);
    const path = '../public/output/HighestNumberOfTimesDismissedPlayer.json';

    // store the data in JSON formate
    fs.writeFileSync(path, JSON.stringify(result, null, 1));
});

// declare findHighestNumberOfTimesDismissedPlayer function
function findHighestNumberOfTimesDismissedPlayer(allDeliveryData){

    // create dismissedPlayer object to store the player wo dismissed by another player
    const dismissedPlayer = {};

    // iterate on allDeliveryData to find player dismissed by another player
    for(let key in allDeliveryData){

        // get the current bowler
        const currentBowler = allDeliveryData[key].bowler;

        // if the player_dismissed is not a empty means we have dismissed player
        if(allDeliveryData[key].player_dismissed !== ""){
            // get the current dismissed player
            const currentDismissedPlayer = allDeliveryData[key].player_dismissed;

            // check the current bolwer is present in dismissedPlayer object or not
            if(dismissedPlayer[currentBowler]){
                // if yes check the current dismissed player is present or not
                if(dismissedPlayer[currentBowler][currentDismissedPlayer]){
                    // if yes just update the dismissed time by one
                    dismissedPlayer[currentBowler][currentDismissedPlayer]++;
                }
                else{
                    // if the current bowler present but dismissed player not present
                    // add dismissed player with value 1
                    dismissedPlayer[currentBowler][currentDismissedPlayer] = 1;
                }
            }
            else{
                // if the bowler is not present in object
                // add bowler with dismissed player with value 1
                dismissedPlayer[currentBowler] = { [currentDismissedPlayer] : 1};
            }
        }
    }

    let dismissedPlayerName;
    let bowler;
    let dismissedCount = 0;
    // iterate on dismissedPlayer object to find highest player dismissed by another player
    for(let key in dismissedPlayer){
        // iterate on dismissedPlayer[key]
        for(let playerName in dismissedPlayer[key]){
            // get the current dismissed time
            const dismissedTime = dismissedPlayer[key][playerName];
            // if the dismissedTime is greater than dismissedCount
            if(dismissedTime > dismissedCount){
                // update the dismissedPlayerName
                dismissedPlayerName = playerName;
                // update the bowler
                bowler = key;
                // update the dismissedCount with new time
                dismissedCount = dismissedTime;
            }
        }
    }
    
    return { 'bowler' : bowler , 'batsman' : dismissedPlayerName , 'number' : dismissedCount}
}