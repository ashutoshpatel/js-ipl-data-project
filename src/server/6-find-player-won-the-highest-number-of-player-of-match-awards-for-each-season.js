// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    allMatchesData.push(data);
})
.on('end', () => {
    
    // call the findHighestNumberOfPlayerOfTheMatchAwardsForEachSeason function
    const result = findHighestNumberOfPlayerOfTheMatchAwardsForEachSeason(allMatchesData);
    const path = '../public/output/playerWonTheHighestNumberOfPlayerOfTheMatch.json';

    // store the data in JSON formate
    fs.writeFileSync(path, JSON.stringify(result, null, 1));
});


// declare findHighestNumberOfPlayerOfTheMatchAwardsForEachSeason function
function findHighestNumberOfPlayerOfTheMatchAwardsForEachSeason(allMatchesData){

    // create empty result object to store final result
    const result = {};
    // create extra object to store the player of match for a season
    const countOfTheMatchAwardsOfPerPlayer = {};

    // use for-in loop to iteate on allMatchesData
    for(let key in allMatchesData){
        // get current season
        const currentSeason = allMatchesData[key].season;
        // get current match player of match
        const currentPlayerOfMatch = allMatchesData[key].player_of_match;

        // check the currentSeason is already present in countOfTheMatchAwardsOfPerPlayer
        if(countOfTheMatchAwardsOfPerPlayer.hasOwnProperty(currentSeason)){
            // if yes then check the currentPlayerOfMatch is present in object or not
            if(countOfTheMatchAwardsOfPerPlayer[currentSeason].hasOwnProperty(currentPlayerOfMatch)){
                // if yes then simply update the value
                countOfTheMatchAwardsOfPerPlayer[currentSeason][currentPlayerOfMatch]++;
            }
            else{
                // if the currentSeason present but the currentPlayerOfMatch not present
                // then add with value
                countOfTheMatchAwardsOfPerPlayer[currentSeason][currentPlayerOfMatch] = 1;
            }
        }
        else{
            // if the currentSeason not present then add in object with currentPlayerOfMatch + value 1
            countOfTheMatchAwardsOfPerPlayer[currentSeason] = {[currentPlayerOfMatch] : 1};
        }
    }

    // now iterate on countOfTheMatchAwardsOfPerPlayer object
    // and find highest number of player of the match awards winning player
    for(let key in countOfTheMatchAwardsOfPerPlayer){
        // get the current season
        const currentSeason = countOfTheMatchAwardsOfPerPlayer[key];
        // create variable to store player name and max count
        let currentPlayerNameMaxTimeWonPlayerOfMatch;
        let countCurrentMaxTimeWonPlayerOfMatch = 0;

        // iteate on current season object
        for(let player in currentSeason){
            // check that current player have more awards then max
            if(currentSeason[player] > countCurrentMaxTimeWonPlayerOfMatch){
                // is yes then update player name and max count
                countCurrentMaxTimeWonPlayerOfMatch = currentSeason[player];
                currentPlayerNameMaxTimeWonPlayerOfMatch = player;
            }
        }

        // add final highest winning player of match awards player in result object
        result[key] = {[currentPlayerNameMaxTimeWonPlayerOfMatch] : countCurrentMaxTimeWonPlayerOfMatch};
    }
    return result;
}