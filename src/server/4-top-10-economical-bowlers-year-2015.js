// importing files
const csv = require('csv-parser');
const fs = require('fs');

const allMatchesData = [];
const allDeliveriesData = [];

fs.createReadStream('../data/matches.csv')
.pipe(csv())
.on('data', (data) => {
    // add only 2015 season data in allMatchesData array
    if(data.season === '2015'){
        allMatchesData.push(data);
    }
})
.on('end', () => {

    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (deliveries) => {
        allDeliveriesData.push(deliveries);
    })
    .on('end', () => {
        // call the findTopTenEconomicalBowlersYear2015 function
        const result = findTopTenEconomicalBowlersYear2015(allMatchesData, allDeliveriesData);
        const path = '../public/output/topTenEconomicalBowlersYear2015.json';

        // store the data in JSON formate
        fs.writeFileSync(path, JSON.stringify(result, null, 1));
    });
});

// declare findTopTenEconomicalBowlersYear2015 function
function findTopTenEconomicalBowlersYear2015(allMatchesData, allDeliveriesData){

    // create object to store bowler name , runs and balls
    const storeBowlerRunsAndBalls = {};

    // iterate on allMatchesData array
    for(let matcheKey in allMatchesData){
        // get the id of matches data
        const matchId = allMatchesData[matcheKey].id;

        for(let deliveryKey in allDeliveriesData){
            // get the id of delivery data
            const deliveryId = allDeliveriesData[deliveryKey].match_id;
            
            // if the matchId and deliveryId are same
            if(matchId == deliveryId){
                // get the current bowler
                const currentBowler = allDeliveriesData[deliveryKey].bowler;
                // get the bye run
                const byeRuns = Number(allDeliveriesData[deliveryKey].bye_runs);
                // get the legbye run
                const legbyeRuns = Number(allDeliveriesData[deliveryKey].legbye_runs);
                // get the total run
                let totalRun = Number(allDeliveriesData[deliveryKey].total_runs);
                // subtract the byeRuns and legbyeRuns from total run
                totalRun = totalRun - byeRuns - legbyeRuns;
                
                // check the bowler is already present in storeBowlerRunsAndBalls object
                if(storeBowlerRunsAndBalls.hasOwnProperty(currentBowler)){
                    // update the runs
                    storeBowlerRunsAndBalls[currentBowler]['runs'] +=  totalRun;
                    // update the balls by one
                    storeBowlerRunsAndBalls[currentBowler]['balls'] +=  1;
                }
                else{
                    // if the bowler is not present in storeBowlerRunsAndBalls object
                    // add them with runs and balls 1
                    storeBowlerRunsAndBalls[currentBowler] = { 'runs' : totalRun , 'balls' : 1};
                }
            }
        }
    }

    // create object to store economy data
    const economyData = {};
    // iterate on storeBowlerRunsAndBalls object to calculate economy
    for(let key in storeBowlerRunsAndBalls){
        // get the current bowler total run
        const run = Number(storeBowlerRunsAndBalls[key]['runs']);
        // get the current bowler total balls
        const balls = Number(storeBowlerRunsAndBalls[key]['balls']) / 6;
        // calculate the economy
        const economy = run / balls;
        // store the economy in economyData object
        economyData[key] = economy.toFixed(2);
    }

    // convert economyData key & values pairs
    const economyDataArray = Object.entries(economyData);
    // sort the economyDataArray
    economyDataArray.sort( (a, b) => {
        const value1 = Number(a[1]);
        const value2 = Number(b[1]);
        return value1 - value2;
    });

    // get the top 10 economy bowler
    const topTenEconomicalBowlers = economyDataArray.slice(0,10);

    // create result object to store top 10 economical bowlers
    const result = Object.fromEntries(topTenEconomicalBowlers);

    // return the result object
    return result;
}